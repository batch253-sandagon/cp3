// import { useEffect, useState, useContext } from 'react';
// import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper,  Typography } from '@mui/material';
// import { Image, Container, Row, Col } from 'react-bootstrap';
// import { IoMdArrowRoundBack } from "react-icons/io";
// import UserContext from '../UserContext';
// import ProductCard from '../components/ProductCard';
// import { NavLink } from 'react-router-dom';

// function MyOrders() {
//   const { setUser } = useContext(UserContext);

//   const [orders, setOrders] = useState([]);
//   const [totalAmounts, setTotalAmounts] = useState([]);
//   const [otherProducts, setOtherProducts] = useState([]);

//   useEffect(() => {
//     fetch(`${process.env.REACT_APP_API_URL}/users/myOrders`, {
//       method: 'GET',
//       headers: {
//         'Authorization': `Bearer ${localStorage.getItem('token')}`
//       }
//     })
//     .then(res => res.json())
//     .then(data => {
//         setOrders(data);
//         setTotalAmounts(data.totalAmount)
//     });
//     }, [setOrders]);

//   useEffect(() => {

// 		fetch(`${process.env.REACT_APP_API_URL}/products`, {
//       method: "POST",
//       headers: {
//           'Content-Type': 'application/json'
//       }
//   })
// 		.then(res => res.json())
// 		.then(data => {
// 			setOtherProducts(data);
// 		})
// 	}, []);

//   const limitedProducts = otherProducts.slice(0, 8);

//   return (
//     <Container>
//         <Typography as={ NavLink } to="/" variant="h4" className="text-dark d-inline-block align-self-center"> 
// 			<IoMdArrowRoundBack />
// 	    </Typography>
//         <Typography style={{fontWeight: "bold"}} variant="h4" className="mt-5 ms-3 d-inline-block">
//             My Order
//         </Typography>
//       <TableContainer component={Paper}>
//         <Table>
//           <TableHead>
//             <TableRow>
//               <TableCell></TableCell>
//               <TableCell style={{ fontSize: "1.2rem" }}>Products</TableCell>
//               <TableCell style={{ fontSize: "1.2rem" }}>Price</TableCell>
//               <TableCell style={{ fontSize: "1.2rem" }}>Stock</TableCell>
//               <TableCell style={{ fontSize: "1.2rem" }}>Quantity</TableCell>
//               <TableCell style={{ fontSize: "1.2rem" }}>Total Amount</TableCell>
//               <TableCell></TableCell>
//             </TableRow>
//           </TableHead>
//           <TableBody>
//             {orders.products.map((product, index) => (
//               <TableRow key={index}>
//                 <TableCell style={{ fontSize: "1.2rem" }}>
//                   <Image fluid style={{ height: '150px' }} src={product.image} alt={product.name}/>
//                 </TableCell>
//                 <TableCell style={{ fontSize: "1.2rem" }}>{product.name}</TableCell>
//                 {/* <TableCell style={{ fontSize: "1.2rem" }}>₱ {product.price.toLocaleString()}</TableCell> */}
//                 <TableCell style={{ fontSize: "1.2rem" }}>{product.stock}</TableCell>
//                 <TableCell>
//                 </TableCell>
//               </TableRow>
//             ))}
//           </TableBody>
//         </Table>
//       </TableContainer>
//         <Row>
//           <Col className='d-flex align-items-center justify-content-end'>
//             <Typography variant="h4" className="mx-3 d-inline">
//               {/* <h3 className='d-inline'>Total Amount:</h3> ₱{products.totalAmout.toLocaleString()} */}
//             </Typography>
//           </Col>
//         </Row>
//       <Typography style={{fontWeight: "bold"}} variant="h5" className='py-3 mt-5'>
//           Other products that you might like
//       </Typography>
//       <Row className='justify-content-around'>
//           {limitedProducts.map(product => (
//             <Col key={product._id} md={3}>
//               <ProductCard product={product} />
//             </Col>
//           ))}
//       </Row>
//     </Container>
//   );
// };

// export default MyOrders;