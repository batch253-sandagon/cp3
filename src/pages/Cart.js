import { useEffect, useState, useContext } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Checkbox,
  Typography,
} from "@mui/material";
import { Image, Container, Row, Col } from "react-bootstrap";
import { MdDelete } from "react-icons/md";
import { IoMdArrowRoundBack } from "react-icons/io";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import { Button, TextField, InputAdornment } from "@mui/material";
import UserContext from "../UserContext";
import ProductCard from "../components/ProductCard";
import { useNavigate } from "react-router-dom";

function Cart() {
  const { setUser } = useContext(UserContext);
  const navigate = useNavigate();

  const [cartCheckoutList, setCartCheckoutList] = useState([]);
  const [products, setProducts] = useState([]);
  const [otherProducts, setOtherProducts] = useState([]);
  const [isChecked, setIsChecked] = useState([]);
  const [quantity, setQuantity] = useState([]);
  const [subtotal, setSubtotal] = useState([]);

  const totalCheckedAmount = isChecked
    .map((checked, index) => (checked ? subtotal[index] : 0))
    .reduce((accumulator, currentValue) => accumulator + currentValue, 0);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setProducts(data.cart);
        setIsChecked(new Array(data.cart.length).fill(false));
        setQuantity(data.cart.map((product) => product.quantity));
        setSubtotal(data.cart.map((product) => product.subtotal));
      });
  }, []);

  const handleCheckboxChange = (event, index) => {
    const newChecked = [...isChecked];
    newChecked[index] = event.target.checked;
    setIsChecked(newChecked);
    const newCartCheckoutList = event.target.checked
      ? [...cartCheckoutList, products[index].productId]
      : cartCheckoutList.filter((id) => id !== products[index].productId);
    setCartCheckoutList(newCartCheckoutList);
  };

  const handleQuantityChange = async (event, index) => {
    const newQuantity = [...quantity];
    newQuantity[index] = event.target.value;
    setQuantity(newQuantity);

    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/users/myCart/update`,
        {
          method: "PATCH",
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            productId: products[index].productId,
            quantity: newQuantity[index],
          }),
        }
      );

      const result = await response.json();
      setSubtotal(result.map((product) => product.subtotal));
    } catch (err) {
      console.log(err);
    }
  };

  const handleIncrement = async (index, stock) => {
    const newQuantity = [...quantity];
    const currentQuantity = parseInt(newQuantity[index]);
    const maxQuantity = parseInt(stock);

    if (currentQuantity < maxQuantity) {
      newQuantity[index] = currentQuantity + 1;
      setQuantity(newQuantity);

      try {
        const response = await fetch(
          `${process.env.REACT_APP_API_URL}/users/myCart/update`,
          {
            method: "PATCH",
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              productId: products[index].productId,
              quantity: newQuantity[index],
            }),
          }
        );

        const result = await response.json();
        setSubtotal(result.map((product) => product.subtotal));
      } catch (err) {
        console.log(err);
      }
    }
  };

  const handleDecrement = async (index) => {
    const newQuantity = [...quantity];
    if (newQuantity[index] > 1) {
      newQuantity[index] = parseInt(newQuantity[index]) - 1;
      setQuantity(newQuantity);

      try {
        const response = await fetch(
          `${process.env.REACT_APP_API_URL}/users/myCart/update`,
          {
            method: "PATCH",
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              productId: products[index].productId,
              quantity: newQuantity[index],
            }),
          }
        );

        const result = await response.json();
        console.log(result);
        setSubtotal(result.map((product) => product.subtotal));
      } catch (err) {
        console.log(err);
      }
    }
  };

  const handleRemove = (index) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/myCart/remove`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        productId: products[index].productId,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setProducts(data);
        console.log(products);
        setUser((prevUser) => ({
          ...prevUser,
          cartCount: data.length,
        }));
      });
  };

  const handleCheckout = async (cartCheckoutList) => {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/users/myCart/checkout`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
          body: JSON.stringify({ checkedProductIds: cartCheckoutList }),
        }
      );
      const data = await response.json();
      if (data) {
        fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
          method: "GET",
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        })
          .then((res) => res.json())
          .then((data) => {
            if (data) {
              toast.success(
                "Order placed successfully!",
                {
                  position: toast.POSITION.TOP_CENTER,
                },
                { autoClose: 1000 }
              );
              setProducts(data.cart);
              setQuantity(data.cart.map((product) => product.quantity));
              setSubtotal(data.cart.map((product) => product.subtotal));
              setIsChecked(new Array(data.cart.length).fill(false));
              setUser({
                id: data._id,
                isAdmin: data.isAdmin,
                cartCount: data.cart.length,
              });
              setCartCheckoutList([])
            } else {
              toast.error(
                "Sorry, there was an error processing your order.",
                {
                  position: toast.POSITION.TOP_CENTER,
                },
                { autoClose: 1000 }
              );
            }
          });
      }
    } catch (error) {
      console.error(error);
    }
  };

  function handleGoBack() {
    navigate(-1); // go back one step in the history stack
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setOtherProducts(data);
      });
  }, []);

  // Toastify Positioning
  const ToastContainerStyle = {
    top: "4.4rem",
    // transform: "translate(-50%, 0%)",
  };

  const limitedProducts = otherProducts.slice(0, 8);

  return (
    <Container>
      <ToastContainer style={ToastContainerStyle} />
      <Typography
        onClick={handleGoBack}
        style={{ cursor: "pointer" }}
        variant="h4"
        className="text-dark d-inline-block align-self-center ms-5 ms-md-0"
      >
        <IoMdArrowRoundBack />
      </Typography>
      <Typography
        style={{ fontWeight: "bold" }}
        variant="h4"
        className="mt-5 mb-4 ms-3 d-inline-block"
      >
        My Cart
      </Typography>

      <TableContainer
        className={products.length === 0 ? "d-none" : ""}
        component={Paper}
      >
        <Table>
          <TableHead>
            <TableRow>
              <TableCell></TableCell>
              <TableCell></TableCell>
              <TableCell style={{ fontSize: "1.2rem" }}>Item</TableCell>
              <TableCell style={{ fontSize: "1.2rem" }}>Price</TableCell>
              <TableCell style={{ fontSize: "1.2rem" }}>Stock</TableCell>
              <TableCell style={{ fontSize: "1.2rem" }}>Quantity</TableCell>
              <TableCell style={{ fontSize: "1.2rem" }}>Subtotal</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {products.map((product, index) => (
              <TableRow key={index}>
                <TableCell style={{ fontSize: "1.2rem" }}>
                  <Checkbox
                    checked={isChecked[index]}
                    onChange={(event) => handleCheckboxChange(event, index)}
                    color="default"
                  />
                </TableCell>
                <TableCell style={{ fontSize: "1.2rem" }}>
                  <Image
                    // fluid
                    style={{ height: "10rem" }}
                    src={product.image.secure_url}
                    alt={product.name}
                  />
                </TableCell>
                <TableCell style={{ fontSize: "1.2rem" }}>
                  {product.name}
                </TableCell>
                <TableCell style={{ fontSize: "1.2rem" }}>
                  ₱ {product.price.toLocaleString()}
                </TableCell>
                <TableCell style={{ fontSize: "1.2rem" }}>
                  {product.stock}
                </TableCell>
                <TableCell>
                  <TextField
                    color="error"
                    sx={{ width: "210px" }}
                    type="text"
                    variant="outlined"
                    value={quantity[index]}
                    onChange={(event) => handleQuantityChange(event, index)}
                    InputProps={{
                      type: "text",
                      step: null,
                      endAdornment: (
                        <InputAdornment position="start">
                          <Button
                            style={{ color: "black" }}
                            onClick={() =>
                              handleIncrement(index, product.stock)
                            }
                          >
                            +
                          </Button>
                        </InputAdornment>
                      ),
                      startAdornment: (
                        <InputAdornment position="end">
                          <Button
                            style={{ color: "black" }}
                            onClick={() => handleDecrement(index)}
                          >
                            -
                          </Button>
                        </InputAdornment>
                      ),
                      inputProps: {
                        style: { textAlign: "center" },
                      },
                      min: 1,
                    }}
                  />
                </TableCell>
                <TableCell style={{ fontSize: "1.2rem" }}>
                  ₱ {subtotal[index].toLocaleString()}
                </TableCell>
                <TableCell>
                  <MdDelete
                    onClick={() => handleRemove(index)}
                    className="fs-4"
                    style={{ color: "red", cursor: "pointer" }}
                  />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      { products.length === 0 && (
        <Typography
        style={{ fontWeight: "bold" }}
        variant="h4"
        className="py-3 mt-1 text-center"
        >
          It's so empty here...
        </Typography>
        )
      }
      {cartCheckoutList.length > 0 && (
        <Row>
          <Col className="d-flex align-items-center justify-content-end me-5 me-md-0">
            <Typography variant="h4" className="mx-3 d-inline">
              <h3 className="d-inline">Total Amount:</h3> ₱
              {totalCheckedAmount.toLocaleString()}
            </Typography>
            <Button
              onClick={() => handleCheckout(cartCheckoutList)}
              style={{
                borderRadius: "10px",
                fontSize: "20px",
              }}
              className="my-4 text-white py-3 px-4"
              variant="contained"
              color="error"
            >
              <Typography variant="h6">Check out</Typography>
            </Button>
          </Col>
        </Row>
      )}
      <Typography
        style={{ fontWeight: "bold" }}
        variant="h5"
        className="py-3 mt-5 mx-5 mx-md-0"
      >
        Other products that you might like
      </Typography>
      <Row className="mx-5 mx-md-0">
        {limitedProducts.map((product) => (
          <Col className="justify-content-around  d-flex" key={product._id} xs={6} lg={4} xl={3}>
            <ProductCard product={product} />
          </Col>
        ))}
      </Row>
    </Container>
  );
}

export default Cart;
