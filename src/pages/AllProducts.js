import { useEffect, useState } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Button,
  Switch,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Typography,
} from "@mui/material";
import { Col, Container, Form, Image, Row } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { FiEdit } from "react-icons/fi";
import { IoMdArrowRoundBack, IoIosAddCircle } from "react-icons/io";

function AllProducts() {
  const navigate = useNavigate();
  const [products, setProducts] = useState([]);
  const [fetchTrigger, setFetchTrigger] = useState(false);
  const [isActive, setIsActive] = useState(false);
  const [openEdit, setOpenEdit] = useState(false);
  const [openAdd, setOpenAdd] = useState(false);
  const [editFormValues, setEditFormValues] = useState({
    id: "",
    name: "",
    description: "",
    category: "",
    price: "",
    stock: "",
    image: "",
  });

  const [addFormValues, setAddFormValues] = useState({
    name: "",
    description: "",
    category: "",
    price: "",
    stock: "",
    image: "",
  });

  const handleClickOpenEdit = (product) => {
    setOpenEdit(true);
    setEditFormValues({
      id: product._id,
      name: product.name,
      description: product.description,
      category: product.category,
      price: product.price,
      stock: product.stock,
      image: product.image.secure_url,
    });
  };

  const handleCloseEdit = () => {
    setOpenEdit(false);
  };

  const handleCloseAdd = () => {
    setOpenAdd(false);
  };

  const handleSave = (event) => {
    event.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/products/${editFormValues.id}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        name: editFormValues.name,
        description: editFormValues.description,
        category: editFormValues.category,
        price: editFormValues.price,
        stock: editFormValues.stock,
        image: editFormValues.image,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          handleCloseEdit();

          // Clear input fields
          setEditFormValues({
            name: "",
            description: "",
            category: "",
            price: "",
            stock: "",
            image: "",
          });
          setFetchTrigger((prev) => !prev);

          toast.success(
            "Product has been updated successfully!",
            {
              position: toast.POSITION.TOP_CENTER,
            },
            { autoClose: 1000 }
          );
        } else {
          handleCloseEdit();

          toast.error(
            "Oops, something went wrong!",
            {
              position: toast.POSITION.TOP_CENTER,
            },
            { autoClose: 1000 }
          );
        }
      });
  };

  const handleInputChangeEdit = (event) => {
    const { id, value, type, files } = event.target;

    // Handle file inputs
    if (type === "file") {
      const file = files[0];
      const reader = new FileReader();
      reader.onload = function (e) {
        // Transform the image here
        const transformedImage = e.target.result;

        // Set the transformed image to the form values
        setEditFormValues({ ...editFormValues, image: transformedImage });
      };

      reader.readAsDataURL(file);
    } else {
      setEditFormValues((prevValues) => ({
        ...prevValues,
        [id]: value,
      }));
    }
  };

  const handleClickArchive = (product) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${product._id}/archive`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          setFetchTrigger((prev) => !prev);
          toast.success(
            `Product has been ${
              product.isActive ? "archived" : "unarchived"
            } successfully!`,
            {
              position: toast.POSITION.TOP_CENTER,
            },
            { autoClose: 1000 }
          );
        } else {
          toast.error(
            "Oops, something went wrong!",
            {
              position: toast.POSITION.TOP_LEFT,
            },
            { autoClose: 1000 }
          );
        }
      });
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setProducts(
          data.map((product) => (
            <TableRow key={product.id}>
              <TableCell style={{ fontSize: "1.2rem", textAlign: "center" }}>
                <Image
                  // fluid
                  style={{ height: "6.2rem" }}
                  src={product.image.secure_url}
                  alt={product.name}
                />
              </TableCell>
              <TableCell style={{ fontSize: "0.9rem" }}>
                {product.name}
              </TableCell>
              <TableCell style={{ fontSize: "0.9rem" }}>
                {product.category}
              </TableCell>
              <TableCell style={{ fontSize: "0.9rem" }}>
                {product.description}
              </TableCell>
              <TableCell style={{ fontSize: "0.9rem" }}>
                ₱ {product.price.toLocaleString()}
              </TableCell>
              <TableCell style={{ fontSize: "0.9rem" }}>
                {product.stock}
              </TableCell>
              <TableCell
                style={{
                  fontSize: "0.9rem",
                  color: product.isActive ? "#4caf50" : "#f44336",
                }}
              >
                {product.isActive ? "Available" : "Unavailable"}
              </TableCell>
              <TableCell>
                <FiEdit
                  style={{ cursor: "pointer" }}
                  className="fs-5"
                  variant="contained"
                  color="success"
                  onClick={() => handleClickOpenEdit(product)}
                >
                  Edit
                </FiEdit>
                <Switch
                  checked={product.isActive}
                  onChange={() => handleClickArchive(product)}
                  name="active-switch"
                  color="default"
                />
              </TableCell>
            </TableRow>
          ))
        );
      });
  }, [fetchTrigger]);

  // Add Products Functions
  const handleInputChangeAdd = (event) => {
    const { id, value, type, files } = event.target;

    // Handle file inputs
    if (type === "file") {
      const file = files[0];
      const reader = new FileReader();
      reader.onload = function (e) {
        // Transform the image here
        const transformedImage = e.target.result;

        // Set the transformed image to the form values
        setAddFormValues({ ...addFormValues, image: transformedImage });
      };

      reader.readAsDataURL(file);
    } else {
      setAddFormValues((prevValues) => ({
        ...prevValues,
        [id]: value,
      }));
    }
  };

  const handleSubmitAdd = (event) => {
    event.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/products/add`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        name: addFormValues.name,
        description: addFormValues.description,
        category: addFormValues.category,
        price: addFormValues.price,
        stock: addFormValues.stock,
        image: addFormValues.image,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          handleCloseAdd();
          // Clear input fields
          setAddFormValues({
            name: "",
            description: "",
            category: "",
            price: "",
            stock: "",
            image: "",
          });
          setFetchTrigger((prev) => !prev);

          toast.success("Added Successfully!", {
            position: toast.POSITION.TOP_CENTER,
          });
        } else {
          handleCloseAdd();

          toast.error("Oops, something went wrong!", {
            position: toast.POSITION.TOP_CENTER,
          });
        }
      });
  };

  const handleClickOpenAdd = () => {
    setOpenAdd(true);
  };

  function handleGoBack() {
    navigate(-1); // go back one step in the history stack
  }

  useEffect(() => {
    // Validation to enable submit button when all fields are populated and both passwords match
    if (
      addFormValues.name !== "" &&
      addFormValues.description !== "" &&
      addFormValues.category !== "" &&
      addFormValues.price &&
      addFormValues.stock !== "" &&
      addFormValues.image !== ""
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [
    addFormValues.name,
    addFormValues.description,
    addFormValues.category,
    addFormValues.price,
    addFormValues.stock,
    addFormValues.image,
  ]);

  // Toastify Positioning
  const ToastContainerStyle = {
    top: "4.4rem",
    // transform: "translate(-50%, 0%)",
  };

  return (
    <Container>
      <ToastContainer style={ToastContainerStyle} />
      <Row className="mt-5 mb-4 mx-5 mx-md-0">
        <Col sm={12} className="d-flex align-items-center">
          <Typography
            onClick={handleGoBack}
            style={{ cursor: "pointer" }}
            variant="h4"
            className="text-dark d-inline"
          >
            <IoMdArrowRoundBack />
          </Typography>
          <Typography
            style={{ fontWeight: "bold" }}
            variant="h4"
            className="ms-3 d-inline-block"
          >
            Product List
          </Typography>
          <Typography
            onClick={() => handleClickOpenAdd()}
            variant="h3"
            className="text-dark ms-auto pe-1 d-inline-block"
          >
            <IoIosAddCircle style={{ color: "red", cursor: "pointer" }} />
          </Typography>
        </Col>
      </Row>
      <TableContainer className='mx-4 mx-md-0' component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell></TableCell>
              <TableCell style={{ fontSize: "1rem" }}>Name</TableCell>
              <TableCell style={{ fontSize: "1rem" }}>Category</TableCell>
              <TableCell style={{ fontSize: "1rem" }}>Description</TableCell>
              <TableCell style={{ fontSize: "1rem" }}>Price</TableCell>
              <TableCell style={{ fontSize: "1rem" }}>Stock</TableCell>
              <TableCell style={{ fontSize: "1rem" }}>Availability</TableCell>
              <TableCell style={{ fontSize: "1rem" }}>Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>{products}</TableBody>
        </Table>
        <Dialog open={openEdit} onClose={handleCloseEdit}>
          <DialogTitle>Edit Product</DialogTitle>
          <DialogContent>
            <Form onSubmit={handleSave}>
              <Form.Group className="mb-3" controlId="name">
                <Form.Label>First Name</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter product name"
                  value={editFormValues.name}
                  onChange={handleInputChangeEdit}
                  required
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="description">
                <Form.Label>Description</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter description"
                  value={editFormValues.description}
                  onChange={handleInputChangeEdit}
                  required
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="category">
                <Form.Label>Category</Form.Label>
                <Form.Select value={editFormValues.category} onChange={handleInputChangeEdit} aria-label="Select Category">
                  <option disabled>Select Category</option>
                  <option required value="Men">Men</option>
                  <option required value="Women">Women</option>
                  <option required value="Kids">Kids</option>
                </Form.Select>
              </Form.Group>

              <Form.Group className="mb-3" controlId="price">
                <Form.Label>Price</Form.Label>
                <Form.Control
                  type="number"
                  placeholder="Enter price"
                  value={editFormValues.price}
                  onChange={handleInputChangeEdit}
                  required
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="stock">
                <Form.Label>Stock</Form.Label>
                <Form.Control
                  type="number"
                  placeholder="Enter stock"
                  value={editFormValues.stock}
                  onChange={handleInputChangeEdit}
                  required
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="image">
                <Form.Label>Product Image</Form.Label>
                <Form.Control
                  type="file"
                  accept="image/*"
                  onChange={handleInputChangeEdit}
                  required
                />
              </Form.Group>
            </Form>
          </DialogContent>
          <DialogActions>
            <Button className="text-dark" onClick={handleCloseEdit}>
              Cancel
            </Button>
            <Button
              style={{ fontWeight: "bold" }}
              className="text-dark"
              onClick={handleSave}
            >
              Save
            </Button>
          </DialogActions>
        </Dialog>

        <Dialog open={openAdd} onClose={handleCloseAdd}>
          <DialogTitle>Add Product</DialogTitle>
          <DialogContent>
            <Form onSubmit={handleSubmitAdd}>
              <Form.Group className="mb-3" controlId="name">
                <Form.Label>First Name</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter product name"
                  value={addFormValues.name}
                  onChange={handleInputChangeAdd}
                  required
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="description">
                <Form.Label>Description</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter description"
                  value={addFormValues.description}
                  onChange={handleInputChangeAdd}
                  required
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="category">
                <Form.Label>Category</Form.Label>
                <Form.Select onChange={handleInputChangeAdd} value={addFormValues.category} aria-label="Select Category">
                  <option disabled>Select Category</option>
                  <option required value="Men">Men</option>
                  <option required value="Women">Women</option>
                  <option required value="Kids">Kids</option>
                </Form.Select>
              </Form.Group>

              <Form.Group className="mb-3" controlId="price">
                <Form.Label>Price</Form.Label>
                <Form.Control
                  type="number"
                  placeholder="Enter price"
                  value={addFormValues.price}
                  onChange={handleInputChangeAdd}
                  required
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="stock">
                <Form.Label>Stock</Form.Label>
                <Form.Control
                  type="number"
                  placeholder="Enter stock"
                  value={addFormValues.stock}
                  onChange={handleInputChangeAdd}
                  required
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="image">
                <Form.Label>Product Image</Form.Label>
                <Form.Control
                  type="file"
                  accept="image/*"
                  onChange={handleInputChangeAdd}
                  required
                />
              </Form.Group>
            </Form>
          </DialogContent>
          <DialogActions>
            <Button className="text-dark" onClick={handleCloseAdd}>
              Cancel
            </Button>
            <Button
              style={{ fontWeight: isActive ? "bold" : "600" }}
              className="text-dark"
              onClick={handleSubmitAdd}
            >
              Save
            </Button>
          </DialogActions>
        </Dialog>
      </TableContainer>
    </Container>
  );
}

export default AllProducts;
