import { Typography } from "@mui/material";
import React from "react";
import { Container } from "react-bootstrap";
import { IoMdArrowRoundBack } from "react-icons/io";
import { useNavigate } from "react-router-dom";

function NotFound() {
  const navigate = useNavigate();

  function handleGoBack() {
    navigate(-1); // go back
  };

  return (
    <Container className="my-5 py-5">
      <Container className="my-4 py-3">
        <Typography
          style={{ cursor: "pointer" }}
          onClick={handleGoBack}
          variant="h4"
          className="text-dark d-inline-block align-self-center"
        >
          <IoMdArrowRoundBack />
        </Typography>
        <Typography
          className="mt-5 pt-5 text-center"
          style={{ fontWeight: "bold" }}
          variant="h1"
        >
          404
        </Typography>
        <Typography
          className="mb-5 pb-5 text-center"
          style={{ fontWeight: "bold" }}
          variant="h3"
        >
          Page Not Found
        </Typography>
      </Container>
    </Container>
  );
}

export default NotFound;
