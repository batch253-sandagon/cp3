import { useState, useEffect, useContext } from "react";
import {
  Button,
  TextField,
  FormControl,
  InputAdornment,
  IconButton,
  Typography,
} from "@mui/material";
import { Visibility, VisibilityOff } from "@mui/icons-material";
import { Navigate, NavLink } from "react-router-dom";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import UserContext from "../UserContext";
import { Card, Col, Container, Row } from "react-bootstrap";

function Login() {
  // Allows us to consume the User context object and it's properties to use for user validation
  const { user, setUser } = useContext(UserContext);

  // State hooks to store the values of the input fields
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  // State to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(true);

  const [showPassword, setShowPassword] = useState(false);

  function authenticate(e) {
    // Prevents page redirection via form submission
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (typeof data.access !== "undefined") {
          localStorage.setItem("token", data.access);
          retrieveUserDetails(data.access);

          localStorage.setItem("isLoggedIn", true);
        } else {
          toast.error(
            "Incorrect username or password. Please try again.",
            {
              position: toast.POSITION.TOP_CENTER,
            },
            { autoClose: 1000 }
          );
        }
      });

    setEmail("");
    setPassword("");
  }

  const retrieveUserDetails = (token) => {
    // The token will be sent as part of the request's header information
    // We put "Bearer" in front of the token to follow implementation standards for JWTs
    fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
          cartCount: data.cart.length,
        });
      });
  };

  useEffect(() => {
    // Validation to enable submit button when all fields are populated and both passwords match
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  // Toastify Positioning
  const ToastContainerStyle = {
    top: "45px",
    // transform: "translate(-50%, 0%)",
  };

  return user.id !== null ? (
    <Navigate to="/" />
  ) : (
    <Container className="my-4 py-5 px-5 text-white">
      <ToastContainer style={ToastContainerStyle} />
      <Row className="mt-4 mb-5 py-5 justify-content-center text-center">
        <Col md={6}>
          <Card
            className="p-5 m-5"
            style={{
              border: "3px solid gray",
              backgroundColor: "#212529",
              borderRadius: "10px",
            }}
          >
            <Typography
              style={{ fontWeight: "bold" }}
              variant="h3"
              className="mt-3 mb-2 d-inline-block"
            >
              Welcome Back
            </Typography>
            <Typography variant="p" className="mb-4 d-inline-block">
              Please enter your details.
            </Typography>
            <form onSubmit={(e) => authenticate(e)}>
              <FormControl fullWidth sx={{ mb: 2 }}>
                <TextField
                  id="outlined-basic"
                  label="Email"
                  variant="outlined"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  InputLabelProps={{
                    style: {
                      color: "white",
                    },
                  }}
                  InputProps={{
                    classes: {
                      focused: "custom-focused",
                      notchedOutline: "custom-notched-outline",
                    },
                    style: {
                      color: "white",
                    },
                  }}
                />
              </FormControl>

              <FormControl fullWidth sx={{ mb: 2 }}>
                <TextField
                  id="outlined-password-input"
                  label="Password"
                  type={showPassword ? "text" : "password"}
                  autoComplete="current-password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  InputLabelProps={{
                    style: {
                      color: "white",
                    },
                  }}
                  InputProps={{
                    classes: {
                      focused: "custom-focused",
                      notchedOutline: "custom-notched-outline",
                    },
                    style: {
                      color: "white",
                    },
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          onClick={() => setShowPassword(!showPassword)}
                          edge="end"
                          style={{ color: "white" }}
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              </FormControl>

              {isActive ? (
                <Button
                  style={{
                    width: "100%",
                    fontSize: "1.1rem",
                    backgroundColor: "#fff",
                    color: isActive ? "#212529" : "#fff",
                  }}
                  variant="contained"
                  type="submit"
                >
                  Log In
                </Button>
              ) : (
                <Button
                  style={{
                    width: "100%",
                    fontSize: "1.1rem",
                    color: "#fff",
                    border: "1px white solid",
                    backgroundColor: "gray",
                  }}
                  variant="contained"
                  type="submit"
                  disabled
                >
                  Log in
                </Button>
              )}
              <Typography variant="p" className="mt-4 mb-4 d-inline-block">
                Don't have an account?{" "}
                <NavLink
                  to="/register"
                  style={{
                    textDecoration: "none",
                    color: "inherit",
                    fontWeight: "bold",
                  }}
                >
                  Sign up
                </NavLink>
              </Typography>
            </form>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

export default Login;
