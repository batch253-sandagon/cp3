import { useState, useEffect, useContext } from 'react';
import { Button, TextField, FormControl, InputAdornment, IconButton, Typography } from "@mui/material";
import { Visibility, VisibilityOff } from "@mui/icons-material";
import { Navigate, NavLink, useNavigate } from 'react-router-dom';
import { toast, ToastContainer } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
import { Card, Col, Container, Row } from 'react-bootstrap';

import UserContext from '../UserContext';

function Register(){

	const { user } = useContext(UserContext);
	const navigate = useNavigate();

	const [showPassword, setShowPassword] = useState(false);

	// State hooks to store the values of the input fields
	// getters are variables that store data (from setters)
	// setters are function that sets the data (for the getters)
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// Function to simulate user registration
	function registerUser(e) {
		// Prevents page redirection via form submission
		e.preventDefault();

    fetch(`${ process.env.REACT_APP_API_URL }/users/register`, {
      method: "POST",
      headers: {
          'Content-Type': 'application/json'
      },
      body: JSON.stringify({
          firstName: firstName,
          lastName: lastName,
          email: email,
          mobileNo: mobileNo,
          password: password1
      })
    })
    .then(res => res.json())
    .then(data => {

      if (data === true) {

          // Clear input fields
          setFirstName('');
          setLastName('');
          setEmail('');
          setMobileNo('');
          setPassword1('');
          setPassword2('');


			toast.success("Registration successful!", {
			position: toast.POSITION.TOP_CENTER
			},
			{ autoClose: 1000 }
			);

          // Allows us to redirect the user to the login page after registering for an account
          navigate("/login");

      } else {

		  toast.error("Duplicate email found", {
			position: toast.POSITION.TOP_CENTER
		},
		{ autoClose: 1000 }
		);

      }
    });
  }

	useEffect(() => {

	    // Validation to enable submit button when all fields are populated and both passwords match
	    if((firstName !== '' && lastName !== '' && email !== '' && mobileNo.length === 11 && password1 !== '' && password2 !== '') && (password1 === password2)){
	        setIsActive(true);
	    } else {
	        setIsActive(false);
	    }

	}, [firstName, lastName, email, mobileNo, password1, password2]);

	// Toastify Positioning
	const ToastContainerStyle = {
		top: '10px',
		// transform: 'translate(-50%, 0%)',
	};

	return(

		(user.id !== null) ?
		    <Navigate to="/" />
		:
			<Container className='px-5 text-white'>
            	<ToastContainer style={ToastContainerStyle} />
				<Row className='mb-5 py-5 justify-content-center text-center'>
				<Col md={6}>
				<Card className='p-5 m-5' style={{ border: '3px solid gray', backgroundColor: "#212529", borderRadius: "10px" }}>
				<Typography style={{fontWeight: "bold"}} variant="h3" className="mt-3 mb-2 d-inline-block">
					Create your account
				</Typography>
				<Typography variant="p" className="mb-4 d-inline-block">
					Please enter your details.
				</Typography>
				<form onSubmit={(e) => registerUser(e)}>
					<Row>
						<Col className='pe-1' sm={6}>
							<FormControl sx={{ mb: 2 }}>
								<TextField 
									id="outlined-basic"
									label="First Name" 
									variant="outlined"
									value={firstName}
									onChange={e => setFirstName(e.target.value)}
									InputLabelProps={{
										style: {
										color: 'white'
										}
									}}
									InputProps={{
										classes: { 
										focused: 'custom-focused',
										notchedOutline: 'custom-notched-outline',
										},
										style: {
										color: 'white'
										}
									}}
							/>
							</FormControl>
						</Col>

						<Col sm={6}>
						<FormControl sx={{ mb: 2 }}>
							<TextField 
								id="outlined-basic"
								label="Last Name" 
								variant="outlined"
								value={lastName}
								onChange={e => setLastName(e.target.value)}
								InputLabelProps={{
									style: {
									color: 'white'
									}
								}}
								InputProps={{
									classes: {
									focused: 'custom-focused',
									notchedOutline: 'custom-notched-outline',
									},
									style: {
									color: 'white'
									}
								}}
						/>
						</FormControl>
						</Col>
					</Row>

					<FormControl fullWidth sx={{ mb: 2 }}>
						<TextField 
							id="outlined-basic"
							label="Mobile No" 
							variant="outlined"
							value={mobileNo}
							onChange={e => setMobileNo(e.target.value)}
							InputLabelProps={{
								style: {
								color: 'white'
								}
							}}
							InputProps={{
								classes: {
								focused: 'custom-focused',
								notchedOutline: 'custom-notched-outline',
								},
								style: {
								color: 'white'
								}
							}}
					/>
					</FormControl>

					<FormControl fullWidth sx={{ mb: 2 }}>
					<TextField 
						id="outlined-basic"
						label="Email" 
						variant="outlined"
						value={email}
						onChange={(e) => setEmail(e.target.value)}
						InputLabelProps={{
							style: {
							color: 'white'
							}
						}}
						InputProps={{
							classes: {
							focused: 'custom-focused',
							notchedOutline: 'custom-notched-outline',
							},
							style: {
							color: 'white'
							}
						}}
					/>
					</FormControl>

					<FormControl fullWidth sx={{ mb: 2 }}>
						<TextField
						id="outlined-password-input"
						label="Password"
						type={showPassword ? "text" : "password"}
						autoComplete="current-password"
						value={password1}
						onChange={(e) => setPassword1(e.target.value)}
						InputLabelProps={{
							style: {
							color: 'white'
							}
						}}
						InputProps={{
							classes: {
							focused: 'custom-focused',
							notchedOutline: 'custom-notched-outline',
							},
							style: {
							color: 'white'
							},
							endAdornment: (
							<InputAdornment position="end">
								<IconButton
								onClick={() => setShowPassword(!showPassword)}
								edge="end"
								style={{color: "white"}}
								>
								{showPassword ? <VisibilityOff /> : <Visibility />}
								</IconButton>
							</InputAdornment>
							),
						}}
						/>
					</FormControl>

					<FormControl fullWidth sx={{ mb: 2 }}>
						<TextField
						id="outlined-password-input"
						label="Password"
						type={showPassword ? "text" : "password"}
						autoComplete="current-password"
						value={password2}
						onChange={(e) => setPassword2(e.target.value)}
						InputLabelProps={{
							style: {
							color: 'white'
							}
						}}
						InputProps={{
							classes: {
							focused: 'custom-focused',
							notchedOutline: 'custom-notched-outline',
							},
							style: {
							color: 'white'
							},
							endAdornment: (
							<InputAdornment position="end">
								<IconButton
								onClick={() => setShowPassword(!showPassword)}
								edge="end"
								style={{color: "white"}}
								>
								{showPassword ? <VisibilityOff /> : <Visibility />}
								</IconButton>
							</InputAdornment>
							),
						}}
						/>
					</FormControl>

					{isActive ? (
						<Button style={{width: "100%", fontSize: "1.1rem", backgroundColor: "#fff", color: isActive ? "#212529" : "#fff"}} variant="contained" type="submit">
						Log In
						</Button>
					) : (
						<Button style={{width: "100%", fontSize: "1.1rem", color: "#fff", border: "1px white solid", backgroundColor: "gray"}} variant="contained" type="submit" disabled>
						Log in
						</Button>
					)}
					<Typography variant="p" className="mt-4 mb-4 d-inline-block">
						Already have an account? <NavLink to="/login" style={{ textDecoration: 'none', color: 'inherit', fontWeight: "bold" }}>Log in</NavLink>
					</Typography>
				</form>
				</Card>
				</Col>
				</Row>
			</Container>
	)
}

export default Register;