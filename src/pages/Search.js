import { useEffect, useState } from "react";
import ProductCard from "../components/ProductCard";
import { Container, Row, Col } from "react-bootstrap";
import { Typography, Skeleton } from "@mui/material";
import { IoMdArrowRoundBack } from "react-icons/io";
import { useNavigate, useParams } from "react-router-dom";

function SearchProducts() {
  const { searchText } = useParams();

  const [products, setProducts] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const navigate = useNavigate();

  function handleGoBack() {
    navigate(-1); // go back one step in the history stack
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        search: `${searchText}`,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        setProducts(
          data.map((product) => {
            return (
              <Col xs={6} lg={4} xl={3} key={product._id} className="d-flex justify-content-around">
                <ProductCard product={product} />
              </Col>
            );
          })
        );
        setIsLoading(false);
      });
  }, [searchText]);

  return (
    <Container>
      <Typography
        style={{ cursor: "pointer" }}
        onClick={handleGoBack}
        variant="h4"
        className="text-dark d-inline ms-5 ms-md-0"
      >
        <IoMdArrowRoundBack />
      </Typography>
      <Typography
        style={{ fontWeight: "bold" }}
        variant="h4"
        className="mt-5 ms-3 d-inline-block"
      >
        Search Result:
      </Typography>
      {isLoading ? (
        <Row className="mx-5 mx-md-0 mt-4">
          { Array.from({ length: 4 }).map((_, index) => (
            <Col xs={6} lg={4} xl={3} key={index} className="d-flex justify-content-around mt-4 pe-1">
              <Skeleton isLoading={true} variant="rounded customSkeleton" width={286} height={344} />
            </Col>
          ))}
        </Row>
      )
        :
        (products.length > 0) ? (
        <Row className="mx-5 mx-md-0 mt-4">{products}</Row>
        ) : (
          <div className="my-3 py-4">
            <div className="my-5 py-5">
              <Typography
                className="my-5 py-5 text-center"
                style={{ fontWeight: "bold" }}
                variant="h3"
              >
                No products found...
              </Typography>
            </div>
          </div>
        )
      }
      
    </Container>
  );
}

export default SearchProducts;
