import { useState, useEffect } from "react";
import { Typography, Button, Card, Skeleton } from "@mui/material";
import { Row, Col, Container, Image, Carousel } from "react-bootstrap";
import { NavLink, useNavigate } from "react-router-dom";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import ProductCard from "../components/ProductCard";
import Faqs from "../components/Faqs";

import HeroIMG from "../assets/images/heroImg.png";
import menIMG from "../assets/images/men.png";
import womenIMG from "../assets/images/women.png";
import kidsIMG from "../assets/images/kids.png";
import product1 from "../assets/images/sl1.png";
import product2 from "../assets/images/sl2.png";
import product3 from "../assets/images/sl3.png";

function Home() {
  const [products, setProducts] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const navigate = useNavigate();

  const handleClick1 = () => {
    navigate("/products");
  };

  const handleClick2 = () => {
    navigate("/products/641f033108870c9e00dff1d4");
  };

  const handleClick3 = () => {
    navigate("/products/641f025108870c9e00dff18f");
  };

  useEffect(() => {
    const isLoggedIn = localStorage.getItem("isLoggedIn");

    if (isLoggedIn) {
      toast.success(
        "Logged in successfully!",
        {
          position: toast.POSITION.TOP_CENTER,
        },
        { autoClose: 1000 }
      );
      localStorage.removeItem("isLoggedIn");
    }
  }, []);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((data) => {
          setProducts(data);
          setIsLoading(false);
      });
  }, []);

  const limitedProducts = products.slice(-4).reverse();

  // Toastify Positioning
  const ToastContainerStyle = {
    top: "4.4rem",
    // transform: "translate(-50%, 0%)",
  };

  return (
    <>
      <Container id="home" className="my-md-4 py-1">
        <ToastContainer style={ToastContainerStyle} />
        <Row className="my-md-5 text-md-start text-center">
          <Col md={6} lg={6} xs={{ span: 12, order: 2 }}>
            <Typography
              variant="h1"
              className="pt-0 mt-0 pt-md-5 mt-md-5 mx-5 mx-md-0"
              style={{ fontWeight: "bold", cursor: "pointer" }}
            >
              Stay Ahead of Time
            </Typography>

            <Typography className=" mx-5 mx-md-0 py-3 py-md-0" variant="h4" style={{ fontWeight: "600" }}>
              Discover Our Smart Watches Collection Today
            </Typography>
            <Button
              onClick={handleClick1}
              style={{
                borderRadius: "10px",
              }}
              className="my-4 text-white py-3 px-4"
              variant="contained"
              color="error"
            >
              <Typography variant="h6">Shop Now</Typography>
            </Button>
          </Col>
          <Col
            md={{ span: 6, order: 2 }}
            lg={6}
            xs={{ span: 8, order: 1 }}
            className="text-center mx-auto my-5 align-self-center"
          >
            <Image fluid src={HeroIMG} />
          </Col>
        </Row>
      </Container>

      <div
        style={{
          backgroundColor: "#212529",
          width: "100%",
          "--bs-gutter-x": "0 !important",
        }}
      >
        <Container className="my-5 py-5">
          <Typography
            style={{ fontWeight: "bold" }}
            className="my-5 text-white mx-5 mx-md-0"
            variant="h3"
          >
            Categories
          </Typography>
          <Row className="category-list justify-content-between mb-5 pb-5 mx-5 mx-md-0">
            <Col>
              <Card className="img-card-container" as={NavLink} to="/mens">
                <div className="img-card">
                  <div className="overlay"></div>
                  <div className="info">
                    <h3>Men's Collection</h3>
                  </div>
                  <img src={menIMG} alt={menIMG} />
                </div>
              </Card>
              <Typography className="text-white d-block d-md-none pt-2" variant="subtitle1" component="div">
                Men's Collection
              </Typography>
            </Col>
            <Col>
              <Card as={NavLink} to="/womens" className="img-card-container">
                <div className="img-card">
                  <div className="overlay"></div>
                  <div className="info">
                    <h3>Women's Collection</h3>
                  </div>
                  <img src={womenIMG} alt={womenIMG} />
                </div>
              </Card>
              <Typography className="text-white d-block d-md-none pt-2" variant="subtitle1" component="div">
                Women's Collection
              </Typography>
            </Col>
            <Col>
              <Card as={NavLink} to="/kids" className="img-card-container">
                <div className="img-card">
                  <div className="overlay"></div>
                  <div className="info">
                    <h3>Kids' Collection</h3>
                  </div>
                  <img src={kidsIMG} alt={kidsIMG} />
                </div>
              </Card>
              <Typography className="text-white d-block d-md-none pt-2" variant="subtitle1" component="div">
              Kids' Collection
              </Typography>
            </Col>
          </Row>
        </Container>
      </div>

      <Container id="hot" className="my-5 py-5">
        <Row className="justify-content-between mx-1 mt-4 mx-5 mx-md-0">
          <Col>
            <Typography
              style={{ fontWeight: "bold" }}
              className="mt-4"
              variant="h3"
            >
              New releases
            </Typography>
          </Col>
          <Col className="d-flex justify-content-end align-items-end">
            <Typography
              as={NavLink}
              to="/products"
              className="text-muted mt-4"
              variant="h6"
            >
              View More
            </Typography>
          </Col>
        </Row>
        <Row className="mx-5 mx-md-0">
          {isLoading ? (
            Array.from({ length: 4 }).map((_, index) => (
              <Col className="mt-4 products" key={index} xs={6} lg={4} xl={3}>
                <Skeleton
                  className="customSkeleton"
                  variant="rounded"
                  width={286}
                  height={344}
                  animation="wave"
                />
              </Col>
            ))
          ) : (
            limitedProducts.map((product) => (
              <Col className="products" key={product._id} xs={6} lg={4} xl={3}>
                <ProductCard product={product} />
              </Col>
            ))
          )}
        </Row>
      </Container>

      <div className="my-5 py-5">
        <Carousel
          className="my-5"
          style={{ backgroundColor: "#212529", color: "white" }}
        >
          <Carousel.Item>
            <Container className="my-5 p-5">
              <Row>
                <Col className="mx-auto" xs={{ span: 8, order: 2 }} md={6}>
                  <Typography
                    style={{ fontWeight: "600" }}
                    className="my-4"
                    variant="h2"
                  >
                    LOW PRICE UP TO 70%
                  </Typography>
                  <Typography
                    style={{ fontWeight: "400" }}
                    className="my-3"
                    variant="h3"
                  >
                    Limited time offer!
                  </Typography>
                  <Button
                    onClick={handleClick1}
                    style={{
                      borderRadius: "10px",
                    }}
                    className="my-3 text-white py-3 px-4"
                    variant="contained"
                    color="error"
                  >
                    <Typography variant="h6">Shop Now</Typography>
                  </Button>
                </Col>
                <Col className="mx-auto text-center" xs={{ span: 7, order: 1 }} md={6}>
                  <Image fluid src={product1} />
                </Col>
              </Row>
            </Container>
          </Carousel.Item>
          <Carousel.Item>
            <Container className="my-5 p-5">
              <Row>
                <Col className="mx-auto text-center" xs={{ span: 6 }} md={6}>
                  <Image fluid src={product2} />
                </Col>
                <Col className="mx-auto text-center" xs={{ span: 8}} md={6}>
                  <Typography
                    style={{ fontWeight: "bold", textAlign: "center" }}
                    className="my-5"
                    variant="h3"
                  >
                    Apple Watch Gen 2
                  </Typography>
                  <hr
                    style={{
                      border: "none",
                      backgroundColor: "white",
                      height: "5px",
                    }}
                  />
                  <Typography
                    className="mt-5"
                    style={{
                      fontWeight: "bold",
                      textDecorationLine: "line-through",
                    }}
                    variant="h5"
                  >
                    ₱ 30,000
                  </Typography>
                  <Typography style={{ fontWeight: "bold" }} variant="h2">
                    ₱ 21,000
                  </Typography>
                  <Button
                    onClick={handleClick2}
                    style={{
                      borderRadius: "10px",
                    }}
                    className="my-4 text-white py-3 px-4"
                    variant="contained"
                    color="error"
                  >
                    <Typography variant="h6">Buy Now</Typography>
                  </Button>
                </Col>
              </Row>
            </Container>
          </Carousel.Item>
          <Carousel.Item>
            <Container className="my-5 p-5">
              <Row>
                <Col className="mx-auto" xs={{ span: 8, order: 2 }} md={6}>
                  <Typography
                    style={{ fontWeight: "bold" }}
                    className="my-4"
                    variant="h2"
                  >
                    Take your watch anywhere
                  </Typography>
                  <Typography
                    style={{ fontWeight: "semi-bold" }}
                    className="my-4"
                    variant="h4"
                  >
                    Water resistance up to 50m with 5ATM certification
                  </Typography>
                  <Button
                    onClick={handleClick3}
                    style={{
                      borderRadius: "10px",
                    }}
                    className="mb-5 text-white py-3 px-4"
                    variant="contained"
                    color="error"
                  >
                    <Typography variant="h6">Buy Now</Typography>
                  </Button>
                </Col>
                <Col className="mx-auto" xs={{ span: 10, order: 1 }} md={6}>
                  <Image fluid className="mb-4" src={product3} />
                </Col>
              </Row>
            </Container>
          </Carousel.Item>
        </Carousel>
      </div>
      <Faqs />
    </>
  );
}

export default Home;
