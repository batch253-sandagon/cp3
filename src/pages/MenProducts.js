import { useEffect, useState } from "react";
import ProductCard from "../components/ProductCard";
import { Container, Row, Col } from "react-bootstrap";
import { Typography, Skeleton } from "@mui/material";
import { IoMdArrowRoundBack } from "react-icons/io";
import { useNavigate } from "react-router-dom";

function MenProducts() {
  const [products, setProducts] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const navigate = useNavigate();

  function handleGoBack() {
    navigate(-1); // go back one step in the history stack
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        search: "Men",
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        setProducts(data);
        setIsLoading(false);
      });
  }, []);

  return (
    <Container>
      <Typography
        onClick={handleGoBack}
        variant="h4"
        className="text-dark d-inline ms-5 ms-md-0"
        style={{ cursor: "pointer" }}
      >
        <IoMdArrowRoundBack />
      </Typography>
      <Typography
        style={{ fontWeight: "bold" }}
        variant="h4"
        className="mt-5 ms-3 d-inline-block"
      >
        Men's Collection
      </Typography>
      <Row className="mt-4">
        {isLoading ? (
            Array.from({ length: 4 }).map((_, index) => (
              <Col xs={6} lg={4} xl={3} key={index} className="d-flex justify-content-around pe-1 mt-4">
                <Skeleton isLoading={true} variant="rounded customSkeleton" width={286} height={344} />
              </Col>
            ))
          ) : (
            products.map((product) => (
              <Col xs={6} lg={4} xl={3} key={product._id} className="d-flex justify-content-around">
                <ProductCard product={product} />
              </Col>
            ))
        )}
      </Row>
    </Container>
  );
}

export default MenProducts;
