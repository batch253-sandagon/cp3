// Built-in react modules imports
import { useState, useEffect } from "react";

import { Container } from "react-bootstrap";
import { BrowserRouter as Router } from "react-router-dom";
import { Route, Routes } from "react-router-dom";

import AppNavbar from "./components/AppNavbar";
import ProductView from "./components/ProductView";
import Footer from "./components/Footer";

import Login from "./pages/Login";
import Cart from "./pages/Cart";
// import MyOrders from "./pages/MyOrders";
import ActiveProducts from "./pages/ActiveProducts";
import AllProducts from "./pages/AllProducts";
import MyProfile from "./pages/MyProfile";
import MenProducts from "./pages/MenProducts";
import NotFound from "./pages/NotFound";
import WomenProducts from "./pages/WomenProducts";
import KidsProducts from "./pages/KidsProducts";
import Register from "./pages/Register";
import Search from "./pages/Search";
import Home from "./pages/Home";
import Logout from "./pages/Logout";

import { UserProvider } from "./UserContext";

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
    cartCount: 0,
  });

  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  };

  // Used to check if the user information is properly stored upon login and the localStorage information is cleared upon logout
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
          cartCount: data.cart.length,
        });
      });
  }, []);

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <Routes>
          <Route
            path="*"
            element={
              <>
                <AppNavbar />
                <Container fluid>
                  <Routes>
                    <Route path="/" element={<Home />} />
                    <Route path="/products" element={<ActiveProducts />} />
                    <Route
                      path="/products/:productId"
                      element={<ProductView />}
                    />
                    <Route path="/allProducts" element={<AllProducts />} />
                    <Route path="/myCart" element={<Cart />} />
                    {/* <Route path="/myOrders" element={<MyOrders />} /> */}
                    <Route path="/search/:searchText" element={<Search />} />
                    <Route path="/mens" element={<MenProducts />} />
                    <Route path="/womens" element={<WomenProducts />} />
                    <Route path="/kids" element={<KidsProducts />} />
                    <Route path="/myProfile" element={<MyProfile />} />
                    <Route path="/logout" element={<Logout />} />
                    <Route path="/*" element={<NotFound />} />
                  </Routes>
                </Container>
                <Footer />
              </>
            }
          />
          <Route path="/register" element={<Register />} />
          <Route path="/login" element={<Login />} />
        </Routes>
      </Router>
    </UserProvider>
  );
}

export default App;
