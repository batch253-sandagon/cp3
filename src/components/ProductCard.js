import { Image, Card } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';

function ProductCard({ product }) {
    const { _id, image, name, price } = product;

    return (
        <Card className="text-dark text-decoration-none col-md-3 mt-4" as={NavLink} to={`/products/${_id}`} style={{ width: '18rem'}}>
        <Card.Body style={{padding: "0"}}>
            <div className='text-center' style={{ height: '15rem', overflow: 'hidden', backgroundColor: "#212529", borderRadius: "10px"  }}>
            <Image fluid className='fluid' src={image.secure_url} alt=".jpg" style={{ height: '100%', objectFit: 'cover' }} />
            </div>
            <div className='my-4 mx-4'>
                <Card.Title>{ name }</Card.Title>
                <Card.Text style={{color: "red"}}>₱ { price.toLocaleString() }</Card.Text>
            </div>
        </Card.Body>
        </Card>
    )
};

export default ProductCard;