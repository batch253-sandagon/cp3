import React from 'react';
import { Accordion, AccordionDetails, AccordionSummary, Typography } from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { Container } from 'react-bootstrap';

const faqs = [
  {
    question: "What is a smartwatch?",
    answer: "A smartwatch is a wearable computer in the form of a wristwatch that allows you to perform various functions, such as receiving notifications, making phone calls, sending messages, and tracking your fitness activity."
  },
  {
    question: "What features do smartwatches offer?",
    answer: "Smartwatches offer a range of features such as GPS tracking, heart rate monitoring, NFC payments, voice commands, music playback, and more."
  },
  {
    question: "Are smartwatches compatible with all smartphones?",
    answer: "Smartwatches are compatible with most smartphones, but it's always a good idea to check the manufacturer's website for compatibility information before making a purchase."
  },
  {
    question: "How do I connect my smartwatch to my phone?",
    answer: "You can connect your smartwatch to your phone via Bluetooth or Wi-Fi. Simply follow the instructions provided by the manufacturer to pair your devices."
  },
  {
    question: "How long does the battery last on a smartwatch?",
    answer: "The battery life of a smartwatch varies depending on the model and usage. Most smartwatches last between 1-3 days on a single charge."
  },
  {
    question: "Can I make phone calls from my smartwatch?",
    answer: "Yes, most smartwatches allow you to make and receive phone calls directly from your wrist, provided they are connected to a compatible smartphone."
  },
  {
    question: "Can I swim with my smartwatch?",
    answer: "Not all smartwatches are waterproof, so you should check the manufacturer's instructions to see if your smartwatch is safe to use while swimming. Even if your smartwatch is water-resistant, it may not be suitable for swimming or other water activities."
  },
  {
    question: "What is the warranty period for a smartwatch?",
    answer: "The warranty period for a smartwatch varies depending on the manufacturer and the model. Most smartwatches come with a one-year warranty, but some may offer longer or shorter warranty periods. It's always a good idea to check the warranty information before making a purchase."
  }
];

function Faqs() {
  return (
    <Container className='pb-5 mb-5'>
      <Typography className='mx-5 mb-3' style={{ fontWeight: "bold" }} variant="h2">FAQs</Typography>
      {faqs.map((faq, index) => (
        <Accordion key={index}>
          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
            <Typography variant="h6">{faq.question}</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              {faq.answer}
            </Typography>
          </AccordionDetails>
        </Accordion>
      ))}
    </Container>
  );
}

export default Faqs;
