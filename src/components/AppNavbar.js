import { useContext, useState, useEffect } from "react";

import {
  Container,
  Navbar,
  Nav,
  Offcanvas,
  Form,
  Button,
  Row,
  Col,
} from "react-bootstrap";
import { Link, NavLink, useNavigate } from "react-router-dom";
import { FiShoppingCart, FiUser } from "react-icons/fi";
import { HiOutlineSearch } from "react-icons/hi";
import {
  Badge,
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Typography,
} from "@mui/material";
import { styled } from "@mui/material/styles";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

import UserContext from "../UserContext";

function AppNavbar() {
  const navigate = useNavigate();
  const { user, setUser } = useContext(UserContext);

  const [showOffcanvas, setShowOffcanvas] = useState(false);
  const [searchText, setSearchText] = useState("");

  const handleOffcanvasClose = () => setShowOffcanvas(false);
  const handleOffcanvasShow = () => setShowOffcanvas(true);

  const StyledBadge = styled(Badge)(({ theme }) => ({
    "& .MuiBadge-badge": {
      right: -3,
      top: 6,
      border: `1px solid ${theme.palette.grey[900]}`,
      backgroundColor: "red",
      color: theme.palette.common.white,
      padding: "0 4px",
      borderColor: "#fff",
    },
  })); 

  const handleSearch = (e) => {
    e.preventDefault();
    // Pass the search text to the URL as a parameter
    setSearchText("");
    navigate(`/search/${searchText}`);
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
          cartCount: data.cart.length,
        });
      });
  }, [setUser]);

  return (
    <Navbar bg="dark" sticky="top">
      <Container>
        <Row
          className="gx-3 align-items-center flex-wrap my-2 mx-5 mx-md-0"
          style={{ width: "100%" }}
        >
          <Col xs={4} md={4} lg={4}>
            <Navbar.Brand className="text-white" as={Link} to="/">
              SmartWrist
            </Navbar.Brand>
          </Col>
          <Col xs={5} md={5} lg={4}>
            <Form onSubmit={handleSearch} className="d-flex">
              <Form.Control
                value={searchText}
                onChange={(e) => setSearchText(e.target.value)}
                type="search"
                placeholder="Search"
                className="me-2 align-self-center"
                aria-label="Search"
                style={{ height: "1.9rem", border: "none", boxShadow: "none" }}
              />
              <Button
                as={Link}
                to={`/search/${searchText}`}
                type="submit"
                variant="outline-dark"
                className="text-white"
              >
                <HiOutlineSearch />
              </Button>
            </Form>
          </Col>
          <Col
            xs={3}
            md={3}
            lg={4}
            className="d-flex flex-row justify-content-end fs-4"
          >
            {/* <Nav className=""> */}
            {user.isAdmin || !user.id ? (
              ""
            ) : (
              <Nav.Link
                as={NavLink}
                to="/myCart"
                className="me-md-4 me-3 text-white"
              >
                <StyledBadge badgeContent={user.cartCount}>
                  <FiShoppingCart />
                </StyledBadge>
              </Nav.Link>
            )}
            <Nav.Link onClick={handleOffcanvasShow} className="text-white">
              <FiUser />
            </Nav.Link>
            {/* </Nav> */}
          </Col>
        </Row>

        {/* </Navbar.Collapse> */}
      </Container>
      <Offcanvas
        show={showOffcanvas}
        onHide={handleOffcanvasClose}
        placement="end"
        style={{ maxWidth: "50%" }}
      >
        <Offcanvas.Header closeButton>
          <Offcanvas.Title className="fs-4">User Menu</Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
          <Nav className="flex-column">
            {/* {
             ( user.id ) ? 
            <Accordion>
              <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                  <Typography variant="h6">My Account</Typography>
              </AccordionSummary>
              <AccordionDetails>
                  <Typography>
                    <Nav.Link as={ NavLink } to="/myProfile" onClick={handleOffcanvasClose}>Profile</Nav.Link>
                    <Nav.Link href="#profile" onClick={handleOffcanvasClose}>Change Password</Nav.Link>
                  </Typography>
              </AccordionDetails>
            </Accordion>
            : 
              ""
            } */}
            {/* <Accordion>
              <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                  <Typography variant="h6">My Orders</Typography>
              </AccordionSummary>
              <AccordionDetails>
                  <Typography>
                    <Nav.Link as={ NavLink } to="/myOrders" onClick={handleOffcanvasClose}>View All Orders</Nav.Link>
                  </Typography>
              </AccordionDetails>
            </Accordion> */}
            {user.isAdmin ? (
              <>
                <Accordion className="mb-3">
                  <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography variant="h6">Products</Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Typography>
                      <Nav.Link
                        as={NavLink}
                        to="/allProducts"
                        onClick={handleOffcanvasClose}
                      >
                        Product Lists
                      </Nav.Link>
                    </Typography>
                  </AccordionDetails>
                </Accordion>
                {/* <Accordion>
                <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography variant="h6">Users</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                      <Nav.Link as={NavLink} to="/addProduct" onClick={handleOffcanvasClose}>All Users</Nav.Link>
                    </Typography>
                </AccordionDetails>
              </Accordion> */}
                {/* <Accordion>
                <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography variant="h6">Orders</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                      <Nav.Link as={NavLink} to="/addProduct" onClick={handleOffcanvasClose}>All Orders</Nav.Link>
                    </Typography>
                </AccordionDetails>
              </Accordion> */}
              </>
            ) : (
              ""
            )}
            {user.id !== null ? (
              <Button
                onClick={handleOffcanvasClose}
                as={NavLink}
                to="/logout"
                style={{
                  width: "100%",
                  color: "#fff",
                  borderRadius: "10px",
                  fontSize: "1.2rem",
                  borderColor: "white",
                  backgroundColor: "#212529",
                }}
                variant="contained"
              >
                Logout
              </Button>
            ) : (
              <>
                <Button
                  onClick={handleOffcanvasClose}
                  as={NavLink}
                  to="/register"
                  style={{
                    width: "100%",
                    color: "#fff",
                    borderRadius: "10px",
                    fontSize: "1.2rem",
                    borderColor: "white",
                    backgroundColor: "#212529",
                  }}
                  variant="contained"
                >
                  Register
                </Button>
                <Button
                  onClick={handleOffcanvasClose}
                  as={NavLink}
                  to="/login"
                  style={{
                    width: "100%",
                    color: "#fff",
                    borderRadius: "10px",
                    fontSize: "1.2rem",
                    borderColor: "white",
                    backgroundColor: "#212529",
                  }}
                  variant="contained"
                >
                  Login
                </Button>
              </>
            )}
          </Nav>
        </Offcanvas.Body>
      </Offcanvas>
    </Navbar>
  );
}

export default AppNavbar;
