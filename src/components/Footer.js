import React from 'react';
import { RiInstagramFill, RiFacebookBoxFill, RiTwitterFill } from "react-icons/ri";
// import { NavLink } from 'react-router-dom';
import "../App.css";

function footer() {
    const year = new Date().getFullYear();
  return (
    <footer>
        <div className="footer-container">
            <div className="about group">
                <h2 className='Logo mb-4'><a href="#" rel="noreferrer">SmartWrist</a></h2>
            </div>
            {/* <div className="hr"></div> */}
            <div className="info group">
                <h3 className='mb-1 mb-lg-4'><a href="/products"  rel="noreferrer">More Collections</a></h3>
                <ul className="p-0">
                    {/* <li><a href='/products'>All</a></li> */}
                    <li><a href="/mens">Men</a></li>
                    <li><a href="/womens">Women</a></li>
                    <li><a href="/kids">Kids</a></li>
                </ul>
            </div>
            {/* <div className="hr"></div> */}
            <div className="follow group">
                <h3 className='mb-1 mb-lg-2'>Follow</h3>
            <ul className="p-0">
                <li><a href="https://www.facebook.com"  target="_blank" rel="noreferrer"><RiFacebookBoxFill/></a></li>
                <li><a href="https://www.instagram.com"  target="_blank" rel="noreferrer"><RiInstagramFill/></a></li>
                <li><a href="http:/twitter.com"  target="_blank" rel="noreferrer"><RiTwitterFill /></a></li>
            </ul>
            </div>
        </div>
        <div className="footer-copyright group">
         <p className='mt-3 mb-0'>&copy; SmartWrist {year}. All rights reserved.</p>
        </div>
    </footer>
  )
}

export default footer