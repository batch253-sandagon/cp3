import { useState, useEffect, useContext } from "react";
import { useParams, NavLink } from "react-router-dom";
import { Container, Row, Col, Offcanvas, Image, Table } from "react-bootstrap";
import UserContext from "../UserContext";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { IoMdArrowRoundBack } from "react-icons/io";

import {
  Typography,
  Button,
  TextField,
  InputAdornment,
  TableContainer,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
} from "@mui/material";

import { useNavigate } from "react-router-dom";

const ProductView = () => {
  const { productId } = useParams();
  const { user, setUser } = useContext(UserContext);
  const [name, setName] = useState("");
  const [price, setPrice] = useState(0);
  const [stock, setStock] = useState(0);
  const [image, setImage] = useState(null);
  const [show, setShow] = useState(false);
  const [quantity, setQuantity] = useState(1);

  const navigate = useNavigate();

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const handleQuantityChange = (event) => {
    setQuantity(event.target.value);
  };

  const handleIncrement = (stock) => {
    setQuantity((prevQuantity) => {
      if (prevQuantity < stock) {
        return prevQuantity + 1;
      }
      return prevQuantity;
    });
  };

  const handleDecrement = () => {
    if (quantity > 1) {
      setQuantity((prevQuantity) => prevQuantity - 1);
    }
  };

  const handleBuyNow = (productId) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/buyNow`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        quantity: quantity,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          handleClose();

          toast.success(
            "Order placed successfully!",
            {
              position: toast.POSITION.TOP_CENTER,
            },
            { autoClose: 1000 }
          );
        } else {
          toast.error(
            "Sorry, there was an error processing your order.",
            {
              position: toast.POSITION.TOP_CENTER,
            },
            { autoClose: 1000 }
          );
        }
      });
  };

  const handleAddToCart = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/addToCart`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin,
            cartCount: data.cart.length,
          });

          toast.success(
            "Added to Cart Successfully!",
            {
              position: toast.POSITION.TOP_CENTER,
            },
            { autoClose: 1000 }
          );
        } else {
          toast.error(
            "Oops, something went wrong!",
            {
              position: toast.POSITION.TOP_CENTER,
            },
            { autoClose: 1000 }
          );
        }
      });
  };

  function handleGoBack() {
    navigate(-1); // go back one step in the history stack
  }

  useEffect(() => {
    window.scrollTo(0, 0);
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        setName(data.name);
        setPrice(data.price);
        setStock(data.stock);
        setImage(data.image.secure_url);
      });
  }, [productId]);

  // Toastify Positioning
  const ToastContainerStyle = {
    top: "4.4rem",
    // transform: "translate(-50%, 0%)",
  };

  return (
    <Container className="my-5 py-5">
      <ToastContainer style={ToastContainerStyle} />
      <Typography style={{ cursor: 'pointer' }} onClick={handleGoBack} variant="h4" className="text-dark d-inline mx-5 mx-md-0">
        <IoMdArrowRoundBack className="mb-4" />
      </Typography>
      <Row>
        <Col
          xs={{span: 4}}
          md={6}
          className="my-5 d-flex justify-content-center align-items-center mx-auto"
        >
          <Image fluid src={image} alt={name} style={{ width: "300px" }} />
        </Col>
        <Col xs={12} md={6} className="mt-5 text-center text-md-start">
          <Typography
            className="mt-4"
            style={{ fontWeight: "bold" }}
            variant="h2"
            gutterBottom
          >
            {name}
          </Typography>
          <Typography className="my-3" variant="h4" gutterBottom>
            <span style={{ fontWeight: "bold" }} className="fs-4">
              Price:{" "}
            </span>{" "}
            <span style={{ color: "red", fontWeight: "bold" }}>
              ₱{price.toLocaleString()}
            </span>
          </Typography>
          <Typography className="mx-5 mx-md-0" style={{ fontWeight: "bold", textAlign: "start" }} variant="h5" gutterBottom>
            Specifications:
          </Typography>
          <TableContainer className="mt-3 px-5 px-md-0">
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell style={{ fontSize: "1.2rem" }}>Display</TableCell>
                  <TableCell style={{ fontSize: "1.2rem" }}>Hardware</TableCell>
                  <TableCell style={{ fontSize: "1.2rem" }}>Battery</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow>
                  <TableCell style={{ fontSize: "1.2rem" }}>
                    1.4-inch OLED display, 400 x 400 pixels
                  </TableCell>
                  <TableCell style={{ fontSize: "1.2rem" }}>
                    Qualcomm Snapdragon Wear 4100, 1GB RAM, 8GB storage
                  </TableCell>
                  <TableCell style={{ fontSize: "1.2rem" }}>
                    340mAh battery, up to 2 days of battery life
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
          {!user || (!user.isAdmin && user.id) ? (
            <Button
              onClick={handleAddToCart}
              style={{
                // width: "190px",
                // height: "56px",
                borderRadius: "10px",
                // fontSize: "20px",
                color: "white",
                borderColor: "white",
                backgroundColor: "black",
              }}
              className="mb-5 text-white me-3 mt-4 py-3 px-4"
              variant="contained"
            >
              <Typography variant="h6">Add to Cart</Typography>
            </Button>
          ) : (
            ""
          )}
          <Button
            onClick={handleShow}
            style={{
              // width: "190px",
              // height: "56px",
              borderRadius: "10px",
              // fontSize: "20px",
            }}
            className="mb-5 text-white mt-4 py-3 px-4"
            variant="contained"
            color="error"
          >
            <Typography variant="h6">Buy Now</Typography>
          </Button>
        </Col>
      </Row>
      <Offcanvas
        show={show}
        onHide={handleClose}
        placement="bottom"
        style={{ height: "60%" }}
      >
        <Container>
          <Offcanvas.Header closeButton>
            <Typography
              style={{ fontWeight: "bold" }}
              variant="h4"
              className="mt-1 d-inline-block"
            >
              Checkout
            </Typography>
          </Offcanvas.Header>
          <Offcanvas.Body>
            <Row>
              <Col xs={5} md={6} className="my-5 d-flex justify-content-center align-items-center">
                <Image fluid src={image} alt={name} />
              </Col>
              <Col xs={7} md={6}>
                <Typography
                  className="mt-5"
                  style={{ fontWeight: "bold" }}
                  variant="h2"
                  gutterBottom
                >
                  {name}
                </Typography>
                <Typography className="my-3" variant="h4" gutterBottom>
                  <span style={{ fontWeight: "bold" }} className="fs-4">
                    Price:{" "}
                  </span>{" "}
                  <span style={{ color: "red", fontWeight: "bold" }}>
                    ₱{price.toLocaleString()}
                  </span>
                </Typography>
                <Typography
                  className="fs-4"
                  style={{ fontWeight: "bold" }}
                  variant="h6"
                  gutterBottom
                >
                  Stock: {stock}
                </Typography>
                <TextField
                  className="my-3"
                  color="error"
                  sx={{
                    width: "210px",
                    border: "1px solid black",
                    borderRadius: "10px",
                  }}
                  type="text"
                  variant="outlined"
                  value={quantity}
                  onChange={handleQuantityChange}
                  InputProps={{
                    type: "text",
                    step: null,
                    endAdornment: (
                      <InputAdornment position="start">
                        <Button
                          style={{ color: "black" }}
                          onClick={() => handleIncrement(stock)}
                        >
                          +
                        </Button>
                      </InputAdornment>
                    ),
                    startAdornment: (
                      <InputAdornment position="end">
                        <Button
                          style={{ color: "black" }}
                          onClick={handleDecrement}
                        >
                          -
                        </Button>
                      </InputAdornment>
                    ),
                    inputProps: {
                      style: { textAlign: "center", fontWeight: "bold" },
                    },
                    min: 1,
                  }}
                />
              </Col>
              {user.id !== null ? (
                !user.isAdmin ? (
                  <Button
                    variant="contained"
                    onClick={() => handleBuyNow(productId)}
                    style={{
                      backgroundColor: "black",
                      color: "white",
                      fontSize: "1.2rem",
                    }}
                  >
                    Place Order
                  </Button>
                ) : (
                  <Button
                    className="text-decoration-none text-center text-white"
                    as={NavLink}
                    to="/logout"
                    variant="contained"
                    style={{
                      backgroundColor: "black",
                      color: "white",
                      fontSize: "1.2rem",
                    }}
                  >
                    Switch Account
                  </Button>
                )
              ) : (
                <Button
                  className="text-decoration-none text-center text-white"
                  as={NavLink}
                  to="/login"
                  variant="contained"
                  style={{
                    backgroundColor: "black",
                    color: "white",
                    fontSize: "1.2rem",
                  }}
                >
                  Log in
                </Button>
              )}
            </Row>
          </Offcanvas.Body>
        </Container>
      </Offcanvas>
    </Container>
  );
};

export default ProductView;
